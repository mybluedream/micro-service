package com.frame.file.utils;

import java.io.File;

import com.module.comm.constants.ConfigCons;

public class FileUtil {
	
	/**
	 * 获取保存文件的前面的绝对路径
	 * @param context
	 * @return
	 */
	public static String getUploadFileDir() {
		String realPath = ConfigCons.projectUploadFilePath;
		String fileDir = null;
		if(realPath == null) {
			realPath = ConfigCons.realpath;
			fileDir = realPath.substring(0, realPath.lastIndexOf(File.separator));
		} else {
			fileDir = realPath;
		}
		fileDir = fileDir + File.separator;
		File dirFile = new File(fileDir);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		return fileDir;
	}
	

	/**
	 * 获取预览文件的路径
	 * @return
	 */
	public static String getPreviewFileDir() {
		String fileDir = ConfigCons.realpath;
		fileDir = fileDir + File.separator + "preview" + File.separator;
		File dirFile = new File(fileDir);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		return fileDir;
	}
}
