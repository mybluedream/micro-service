package com.module.admin.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.module.admin.BaseController;

/**
 * 用户信息的操作
 * @author yuejing
 * @date 2016-05-22 11:17:54
 * @version V1.0.0
 */
@Controller
public class SysResController extends BaseController {

	/**
	 * 管理中心
	 */
	@RequestMapping(value = "/sysRes/f-view/manager")
	@RequiresPermissions(value="ADMIN_GRANT_RES")
	public String main(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		/*String menuIds = sysRoleService.get(userInfo.getRoleId()).getGrantids();
		List<SysMenu> menuList = sysMenuService.findTreeByMenuIds(menuIds);
		modelMap.put("menuList", menuList);
		
		setSession(request, SessionCons.USER_MENU, sysMenuService.findByMenuIds(menuIds));*/
		return "admin/sys/sysRes-manager";
	}
	
}