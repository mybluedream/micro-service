package com.system.template.test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.system.template.TemplateHandler;

/**
 * 页面生成横向的pdf
	在html的style里面加入pdf能识别的样式
	@page{}这个就是与其他样式区别开来的标志，例如这里面写@page{size:297mm 210mm;}
	这个就表示纸张的宽是297毫米，高是210毫米，这样打印出来的效果就跟横着的A4纸一样了,一般放在style第一行。
 * @author yuejing
 * @date 2018年3月23日 上午11:00:18
 */
public class PdfTemplateTest {
	
	public static void main(String[] args) {
		TemplateHandler handler = TemplateHandler.createPdf();
		
		handler.addValue("title", "这是一个好标题");

		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		for (int i = 0; i < 60; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			for (int j = 0; j < 10; j++) {
				map.put("col" + j, "行" + i + "列" + j);
			}
			list.add(map);
		}
		handler.addValue("list", list);
		String baseDir = "D:\\git\\micro-service\\ms-core\\comm\\plug-template\\src\\test\\java\\";
		String templatePath = baseDir + "pdf.ftl";
		String createPath = baseDir + "pdf.pdf";
		//生成pdf	模版内必须指定<body style="font-family:'Arial Unicode MS'">
		File file = handler.generatePdf(templatePath, createPath);
		System.out.println(file);
	}
}
