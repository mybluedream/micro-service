package com.ms.task.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.ms.task.interceptor.AdminSecurityInterceptor;
import com.ms.task.interceptor.AuthSecurityInterceptor;

/**
 * WebMvc的配置
 * @author yuejing
 * @date 2019年3月5日 下午1:15:08
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 添加拦截器
		registry.addInterceptor(new AuthSecurityInterceptor())
		.addPathPatterns("/api/*/*").excludePathPatterns("/refresh");
		AdminSecurityInterceptor admin = new AdminSecurityInterceptor();
		admin.setLoginUrl("/index.jsp?errorcode=1");
		registry.addInterceptor(admin)
		.addPathPatterns("/*/f-view/**", "/*/f-json/**").excludePathPatterns("/refresh");
		WebMvcConfigurer.super.addInterceptors(registry);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/view/**").addResourceLocations("/view/");
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}
	
	@Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        //setUseSuffixPatternMatch 后缀模式匹配
        configurer.setUseSuffixPatternMatch(true);
        //setUseTrailingSlashMatch 自动后缀路径模式匹配
        configurer.setUseTrailingSlashMatch(true);
    }
}
