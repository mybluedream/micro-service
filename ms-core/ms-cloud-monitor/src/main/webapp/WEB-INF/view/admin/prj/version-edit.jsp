<%@page import="com.module.admin.sys.enums.SysFileType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑项目版本</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
		<input type="hidden" id="prjId" value="${param.prjId}">
		<div class="form-group">
			<label for="version" class="col-sm-4">版本号 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="version" placeholder="版本号" value="${prjVersion.version}"></div>
		</div>
		<div class="form-group">
			<label for="isRelease" class="col-sm-4">发布 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:radio id="isRelease" name="isRelease" dictcode="boolean" value="${prjVersion.isRelease}" defvalue="1"/></div>
		</div>
		<div class="form-group">
			<label for="rbVersion" class="col-sm-4">参考版本 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="rbVersion" headerKey="" headerValue="参考版本" items="${list}" value="${prjVersion.rbVersion}" cssCls="form-control" /></div>
		</div>
		<div class="form-group">
			<label for="isRelTime" class="col-sm-4">定时发布 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:radio id="isRelTime" name="isRelTime" dictcode="boolean" value="${prjVersion.isRelTime}" defvalue="0" exp="onClick=\"info.changeRelTime(this)\""/></div>
		</div>
		<div class="form-group" id="relTimePanel" style="display: none;">
			<label for="relTime" class="col-sm-4">定时发布</label>
			<div class="col-sm-8 date form_datetime">
				<input size="16" type="text" class="form-control" id="relTime" placeholder="请选择发布时间" value="<my:date value="${prjVersion.relTime}" pattern="yyyy-MM-dd HH:mm:ss"/>">
    			<span class="add-on"><i class="icon-th"></i></span>
			</div>
		</div>
		<div class="form-group">
			<label for="remark" class="col-sm-4 va-top">备注</label>
			<div class="col-sm-8">
				<textarea class="form-control" id="remark" rows="3" cols="" placeholder="备注">${prjVersion.remark}</textarea>
			</div>
		</div>
		<div id="deployPanel">
			<input type="hidden" id="deployType" value="${prjVersion.deployType}"/>
			<ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#packagePanel" onclick="$('#deployType').val('10')" role="tab" data-toggle="tab">发布包</a></li>
			    <li role="presentation"><a href="#gitPanel" onclick="$('#deployType').val('20')" role="tab" data-toggle="tab">Git部署</a></li>
			</ul>
			
			<!-- Tab panes -->
			<div class="tab-content">
			    <div role="tabpanel" class="tab-pane active padding-20" id="packagePanel">
			    	<div class="form-group">
						<label for="rbVersion" class="col-sm-4">版本上传 <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="file" id="files" name="files" onchange="fi.uploadFile()"/><img id="filesLoading" alt="上传中..." src="${webroot}/resources/images/loading.gif" style="display: none;"/>
							<input type="hidden" id="pathUrl" name="pathUrl" value="${prjVersion.pathUrl}"/>
						</div>
					</div>
					<div class="form-group" id="pathUrlPanel">
						<label for="download" class="col-sm-4"></label>
						<div class="col-sm-8">
							<c:choose>
							<c:when test="${prjVersion == null}"><span class="text-info">请上传项目</span></c:when>
							<c:otherwise>
								<a href="${webroot}/sysFile/f-view/download.shtml?url=${prjVersion.pathUrl}" target="_blank">下载项目</a>
							</c:otherwise>
							</c:choose>
						</div>
					</div>
			    </div>
			    <div role="tabpanel" class="tab-pane padding-20" id="gitPanel">
			    	<p>先决条件：保证Monitor服务器上安装好了git客户端和Maven客户端。</p>
					<div class="form-group">
						<label for="remoteRepoUri" class="col-sm-4">下载地址 <span class="text-danger">*</span></label>
						<div class="col-sm-8"><input type="text" class="form-control" id="remoteRepoUri" placeholder="git远程下载的地址" value=""></div>
					</div>
			    	<div class="form-group">
						<label for="localRepoPath" class="col-sm-4">本地仓库 <span class="text-danger">*</span></label>
						<div class="col-sm-8"><input type="text" class="form-control" id="localRepoPath" placeholder="git的本地仓库地址" value=""></div>
					</div>
			    	<div class="form-group">
						<label for="branchName" class="col-sm-4">分支名称 <span class="text-danger">*</span></label>
						<div class="col-sm-8"><input type="text" class="form-control" id="branchName" placeholder="git的分支名称，如master等" value=""></div>
					</div>
			    	<div class="form-group">
						<label for="gitUsername" class="col-sm-4">用户名 <span class="text-danger">*</span></label>
						<div class="col-sm-8"><input type="text" class="form-control" id="gitUsername" placeholder="git的登录用户名" value=""></div>
					</div>
			    	<div class="form-group">
						<label for="gitPassword" class="col-sm-4">密码 <span class="text-danger">*</span></label>
						<div class="col-sm-8"><input type="text" class="form-control" id="gitPassword" placeholder="git的登录密码" value=""></div>
					</div>
			    </div>
			</div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/upload.jsp"></jsp:include>
	<script type="text/javascript">
	var fi = {
			uploadFile : function() {
				upload.file({
					fileId : 'files',
					loading : 'filesLoading',
					url : '${webroot}/sysFile/f-json/upload.shtml',
					param : {
						filename : 'files', type : '<%=SysFileType.PRJ.getCode()%>'
					},
					success : function(data) {
						$('#pathUrl').val(data.body.url);
						$('#pathUrlPanel').empty().append(['<label for="download" class="col-sm-4"></label>',
						                   					'<div class="col-sm-8"><a href="',webroot,'/sysFile/f-view/download.shtml?url=',data.body.url,'" target="_blank">下载项目</a>',
						                   					'</div>'].join(''));
					}
				});
			}
	};
	var info = {
			changeRelTime: function(_this) {
				var value = $(_this).val();
				var display = 'block';
				if (value == 0) {
					display = 'none';
				}
				$('#relTimePanel').css('display', display);
			}
	};
	$(function() {
		$('.form_datetime').datetimepicker({
			format: 'yyyy-mm-dd hh:ii:ss',
			autoclose: true,
			todayHighlight: true,
			startView: 1
		});
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			
			var _prjId = $('#prjId').val();
			var _version = $('#version');
			if(JUtil.isEmpty(_version.val())) {
				_saveMsg.append('请输入版本号');
				_version.focus();
				return;
			}
			var _isRelease = $('input[name="isRelease"]:checked');
			/* if(JUtil.isEmpty(_isRelease.val())) {
				_saveMsg.append('请选择是否发布');
				_isRelease.focus();
				return;
			} */
			var _pathUrl = $('#pathUrl');
			if(JUtil.isEmpty(_pathUrl.val())) {
				_saveMsg.append('请上传项目');
				return;
			}
			
			var _rbVersion = $('#rbVersion');
			
			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/prjVersion/f-json/save.shtml',
				data : {
					prjId: _prjId,
					version: _version.val(),
					remark: $('#remark').val(),
					isRelease: _isRelease.val(),
					rbVersion: _rbVersion.val(),
					pathUrl: _pathUrl.val(),
					isRelTime: $('input[name="isRelTime"]:checked').val(),
					relTime: $('#relTime').val(),
					deployType: $('#deployType').val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>