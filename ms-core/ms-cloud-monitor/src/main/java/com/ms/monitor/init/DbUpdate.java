package com.ms.monitor.init;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.system.comm.utils.FrameStringUtil;
import com.system.dao.BaseDao;
import com.system.dao.utils.DbUtil;
import com.system.ds.DbContextHolder;

/**
 * 脚本升级
 * @author yuejing
 * @date 2018年12月18日 上午11:17:25
 */
public class DbUpdate {

	private static final Logger LOGGER = LoggerFactory.getLogger(DbUpdate.class);

	/**
	 * 初始化更新后的脚本
	 */
	public static void init() {
		/*if (EnvUtil.projectDbUpdate() != 1) {
			return;
		}*/
		/*ThreadPoolTaskExecutor task = FrameSpringBeanUtil.getBean(ThreadPoolTaskExecutor.class);
		task.execute(new Runnable() {

			@Override
			public void run() {*/
		updateSql();
		/*}

		});*/
	}
	private static void updateSql() {
		DbContextHolder.setDsName("dataSource1");
		BaseDao baseDao = new BaseDao();
		if (DbUtil.isMysql()) {
			version1_0_0(baseDao);
			//version1_0_1(baseDao);
		} else if (DbUtil.isOracle()) {
			// TODO 带扩展Oracle的初始化脚本
		}

	}
	
	/*private static void version1_0_1(BaseDao baseDao) {
	String sql = null;
	// 任务表增加字段: 失败发送邮件规则
	List<TableField> fields = baseDao.tableFields("task_job");
	boolean isFailemailrule = false;
	for (TableField f : fields) {
		if ("failemailrule".equalsIgnoreCase(f.getName())) {
			isFailemailrule = true;
		}
	}
	if (!isFailemailrule) {
		LOGGER.info("|===================================================");
		LOGGER.info("|========== 表[task_job]增加字段: failemailrule");
		sql = "alter table task_job add failemailrule varchar(250) comment '失败发送邮件规则'";
		baseDao.updateSql(sql);
		LOGGER.info("|===================================================");
	}

}*/
	
	private static void version1_0_0(BaseDao baseDao) {
		createCliInfo(baseDao);
		createCodeCreate(baseDao);
		createCodePrj(baseDao);
		createCodeTemplate(baseDao);
		createMsConfig(baseDao);
		createMsConfigValue(baseDao);
		createMsSecret(baseDao);
		createMsSecretApi(baseDao);
		createPrjAnt(baseDao);
		createPrjApi(baseDao);
		createPrjApiTest(baseDao);
		createPrjApiTestDtl(baseDao);
		createPrjClient(baseDao);
		createPrjDs(baseDao);
		createPrjInfo(baseDao);
		createPrjMonitor(baseDao);
		createPrjOptimize(baseDao);
		createPrjOptimizeLog(baseDao);
		createPrjVersion(baseDao);
		createPrjVersionScript(baseDao);
		createSysConfig(baseDao);
		createSysFile(baseDao);
		createSysUser(baseDao);
		createSysUserAtt(baseDao);
	}
	private static void createSysUserAtt(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from sys_user_att limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[sys_user_att]: " + e.getMessage());
			String sql = "CREATE TABLE `sys_user_att` (\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '用户编号',\r\n" + 
					"  `type` varchar(20) NOT NULL COMMENT '类型[10项目]',\r\n" + 
					"  `type_no` varchar(32) NOT NULL COMMENT '类型编码',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  PRIMARY KEY  (`user_id`,`type`,`type_no`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的关注表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createSysUser(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from sys_user limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[sys_user]: " + e.getMessage());
			String sql = "CREATE TABLE `sys_user` (\r\n" + 
					"  `user_id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `username` varchar(30) NOT NULL COMMENT '用户名',\r\n" + 
					"  `password` varchar(80) NOT NULL COMMENT '密码',\r\n" + 
					"  `nickname` varchar(30) NOT NULL COMMENT '昵称',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[10正常、20冻结]',\r\n" + 
					"  PRIMARY KEY  (`user_id`),\r\n" + 
					"  UNIQUE KEY `unique_username` (`username`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';";
			sql += "insert  into `sys_user`(`user_id`,`username`,`password`,`nickname`,`create_time`,`status`) values (1,'admin','43286a86708820e38c333cdd4c496355','admin','2016-10-19 10:53:38',10);";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createSysFile(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from sys_file limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[sys_file]: " + e.getMessage());
			String sql = "CREATE TABLE `sys_file` (\r\n" + 
					"  `file_id` varchar(32) NOT NULL COMMENT '编号',\r\n" + 
					"  `type` int(11) NOT NULL COMMENT '类型[10项目]',\r\n" + 
					"  `org_name` varchar(80) NOT NULL COMMENT '原名称',\r\n" + 
					"  `sys_name` varchar(80) NOT NULL COMMENT '系统名称',\r\n" + 
					"  `url` varchar(200) NOT NULL COMMENT '显示路径',\r\n" + 
					"  `file_type` varchar(20) NOT NULL COMMENT '文件类型',\r\n" + 
					"  `file_size` float NOT NULL COMMENT '文件大小',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[0待确定、1使用中、2未使用、3已作废]',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  PRIMARY KEY  (`file_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统文件表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createSysConfig(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from sys_config limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[sys_config]: " + e.getMessage());
			String sql = "CREATE TABLE `sys_config` (\r\n" + 
					"  `code` varchar(50) NOT NULL COMMENT '编码',\r\n" + 
					"  `name` varchar(50) NOT NULL COMMENT '名称',\r\n" + 
					"  `value` varchar(100) NOT NULL COMMENT '值',\r\n" + 
					"  `remark` varchar(100) default NULL COMMENT '描叙',\r\n" + 
					"  `exp1` varchar(100) default NULL COMMENT '扩展1',\r\n" + 
					"  `exp2` varchar(100) default NULL COMMENT '扩展2',\r\n" + 
					"  PRIMARY KEY  (`code`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表';";
			sql += "insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.smtp','发送邮箱的smtp','smtp.163.com',null,null,null);\r\n" + 
					"insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.from','发送邮件的邮箱','xxx@163.com',null,null,null);\r\n" + 
					"insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.username','发送邮件的用户名','xxxx',null,null,null);\r\n" + 
					"insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.password','发送邮件的密码','xxxxxx',null,null,null);\r\n" + 
					"insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.send.is.open','是否打开发送邮件的功能[0否、1是]','0',null,null,null);\r\n" + 
					"insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('prj.file.path','项目上传的目录','/home/monitor/file',null,null,null);\r\n" + 
					"insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('prj.monitor.fail.email','项目检测失败接收的邮箱','yuejingjiahong@163.com',null,null,null);";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjVersionScript(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_version_script limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_version_script]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_version_script` (\r\n" + 
					"  `pvs_id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `version` varchar(32) NOT NULL COMMENT '版本号',\r\n" + 
					"  `remark` varchar(100) default NULL COMMENT '备注',\r\n" + 
					"  `ds_code` varchar(32) NOT NULL COMMENT '数据源编号',\r\n" + 
					"  `up_sql` text NOT NULL COMMENT '升级脚本',\r\n" + 
					"  `callback_sql` text COMMENT '回滚脚本',\r\n" + 
					"  `is_up` int(11) NOT NULL COMMENT '是否升级',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  PRIMARY KEY  (`pvs_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目版本脚本表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjVersion(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_version limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_version]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_version` (\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `version` varchar(50) NOT NULL COMMENT '版本号',\r\n" + 
					"  `remark` varchar(300) default NULL COMMENT '描叙',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  `is_release` int(11) NOT NULL COMMENT '是否发布',\r\n" + 
					"  `path_url` varchar(200) NOT NULL COMMENT '版本所在的路径',\r\n" + 
					"  `rb_version` varchar(50) default NULL COMMENT '回滚版本',\r\n" + 
					"  `is_rel_time` int(11) NOT NULL COMMENT '是否定时发布',\r\n" + 
					"  `rel_time` datetime NOT NULL COMMENT '定时发布时间',\r\n" + 
					"  `rel_status` int(11) NOT NULL COMMENT '定时发布状态[10待发布、20发布中，30发布失败、40发布成功]',\r\n" + 
					"  `rel_msg` varchar(250) default NULL COMMENT '定时发布结果',\r\n" + 
					"  `deploy_type` int(11) default '10' NOT NULL COMMENT '部署类型[10发布包、20GIT部署]',\r\n" + 
					"  `git_info` text default NULL COMMENT 'git部署信息',\r\n" + 
					"  PRIMARY KEY  (`prj_id`,`version`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目版本表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjOptimizeLog(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_optimize_log limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_optimize_log]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_optimize_log` (\r\n" + 
					"  `id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `url` varchar(200) NOT NULL COMMENT '接口地址',\r\n" + 
					"  `params` text COMMENT '请求参数',\r\n" + 
					"  `req_time` datetime NOT NULL COMMENT '请求时间',\r\n" + 
					"  `res_time` datetime NOT NULL COMMENT '响应时间',\r\n" + 
					"  `use_time` int(11) default NULL COMMENT '用时[单位ms]',\r\n" + 
					"  `res_result` longtext COMMENT '响应结果',\r\n" + 
					"  `res_size` float default NULL COMMENT '响应数据大小[单位kb]',\r\n" + 
					"  `remark` text COMMENT '备注',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  PRIMARY KEY  (`id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目优化记录表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjOptimize(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_optimize limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_optimize]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_optimize` (\r\n" + 
					"  `id` int(11) NOT NULL auto_increment COMMENT '编码',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `url` varchar(200) NOT NULL COMMENT '接口地址',\r\n" + 
					"  `warn_time` int(11) NOT NULL COMMENT '预警时间[单位ms]',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  PRIMARY KEY  (`id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目优化规则表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjMonitor(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_monitor limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_monitor]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_monitor` (\r\n" + 
					"  `prjm_id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `type` int(11) NOT NULL COMMENT '监控类型[10服务、20数据库、30缓存、40其它]',\r\n" + 
					"  `remark` varchar(100) NOT NULL COMMENT '描叙',\r\n" + 
					"  `monitor_is` int(11) NOT NULL COMMENT '是否检测',\r\n" + 
					"  `monitor_succ_str` varchar(100) default NULL COMMENT '检测成功标识',\r\n" + 
					"  `monitor_status` int(11) default NULL COMMENT '检测状态[10正常、20异常]',\r\n" + 
					"  `monitor_url` varchar(150) default NULL COMMENT '检测地址',\r\n" + 
					"  `monitor_time` datetime default NULL COMMENT '检测时间',\r\n" + 
					"  `monitor_fail_num` int(11) default NULL COMMENT '检测失败次数',\r\n" + 
					"  `monitor_fail_num_remind` int(11) default NULL COMMENT '检测失败最大次数提醒[0代表不提醒]',\r\n" + 
					"  `monitor_fail_email` varchar(50) default NULL COMMENT '检测失败接收邮箱',\r\n" + 
					"  `monitor_fail_send_time` datetime default NULL COMMENT '检测失败发送信息时间',\r\n" + 
					"  `monitor_fail_send_interval` int(11) default NULL COMMENT '检测失败发送信息间隔[单位：分钟]',\r\n" + 
					"  PRIMARY KEY  (`prjm_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目监控表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjInfo(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_info limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_info]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_info` (\r\n" + 
					"  `prj_id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `code` varchar(50) NOT NULL COMMENT '项目编码',\r\n" + 
					"  `name` varchar(100) NOT NULL COMMENT '名称',\r\n" + 
					"  `remark` varchar(200) default NULL COMMENT '描叙',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  `release_version` varchar(50) default NULL COMMENT '发布的版本号',\r\n" + 
					"  `release_time` datetime default NULL COMMENT '发布的版本时间',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[10正常、20停用]',\r\n" + 
					"  `container` int(11) NOT NULL COMMENT '容器类型[10tomcat、50自定义服务、100其它]',\r\n" + 
					"  `shell_script` text COMMENT 'shell脚本',\r\n" + 
					"  `monitor_status` int(11) NOT NULL default '0' COMMENT '监控状态是否正常',\r\n" + 
					"  `monitor_msg` varchar(200) default NULL COMMENT '监控消息',\r\n" + 
					"  PRIMARY KEY  (`prj_id`),\r\n" + 
					"  UNIQUE KEY `UQ_CODE` (`code`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjDs(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_ds limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_ds]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_ds` (\r\n" + 
					"  `code` varchar(32) NOT NULL COMMENT '数据源编码',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `type` varchar(30) NOT NULL COMMENT '数据库类型[mysql/oracle]',\r\n" + 
					"  `driver_class` varchar(100) NOT NULL COMMENT '驱动类',\r\n" + 
					"  `url` varchar(100) NOT NULL COMMENT 'jdbc的url',\r\n" + 
					"  `username` varchar(50) NOT NULL COMMENT '用户名',\r\n" + 
					"  `password` varchar(50) NOT NULL COMMENT '密码',\r\n" + 
					"  `initial_size` int(11) NOT NULL COMMENT '初始连接数',\r\n" + 
					"  `max_idle` int(11) NOT NULL COMMENT '最大连接数',\r\n" + 
					"  `min_idle` int(11) NOT NULL COMMENT '最小连接数',\r\n" + 
					"  `test_sql` varchar(200) NOT NULL COMMENT '测试的sql语句',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  PRIMARY KEY  (`code`,`prj_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目数据源表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjClient(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_client limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_client]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_client` (\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `client_id` varchar(32) NOT NULL COMMENT '客户端编号',\r\n" + 
					"  `version` varchar(50) NOT NULL COMMENT '版本编号',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[10待发布、20发布中、30发布失败、40发布成功]',\r\n" + 
					"  `status_msg` varchar(200) default NULL COMMENT '状态消息',\r\n" + 
					"  `release_time` datetime default NULL COMMENT '发布时间',\r\n" + 
					"  `shell_script` text COMMENT '客户端执行的Shell命令',\r\n" + 
					"  `log_path` varchar(150) default NULL COMMENT '日志路径',\r\n" + 
					"  PRIMARY KEY  (`prj_id`,`client_id`,`version`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目客户端表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjApiTestDtl(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_api_test_dtl limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_api_test_dtl]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_api_test_dtl` (\r\n" + 
					"  `pat_id` int(11) NOT NULL COMMENT '项目API测试编号',\r\n" + 
					"  `path` varchar(150) NOT NULL COMMENT '项目API路径',\r\n" + 
					"  `params` text NOT NULL COMMENT '参数',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[10待测试、20测试中、30成功、40失败]',\r\n" + 
					"  `succ_cond` varchar(255) NOT NULL COMMENT '成功条件',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `test_time` datetime default NULL COMMENT '测试时间',\r\n" + 
					"  `test_result` varchar(255) default NULL COMMENT '测试结果',\r\n" + 
					"  PRIMARY KEY  (`pat_id`,`path`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目API测试接口表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjApiTest(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_api_test limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_api_test]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_api_test` (\r\n" + 
					"  `id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `name` varchar(100) NOT NULL COMMENT '测试名称',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `test_time` datetime default NULL COMMENT '测试时间',\r\n" + 
					"  `test_result` varchar(255) default NULL COMMENT '测试结果',\r\n" + 
					"  PRIMARY KEY  (`id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目API测试表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjApi(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_api limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_api]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_api` (\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `path` varchar(255) NOT NULL COMMENT '路径',\r\n" + 
					"  `name` varchar(200) default NULL COMMENT '名称',\r\n" + 
					"  `method` varchar(500) NOT NULL COMMENT '方法详情',\r\n" + 
					"  `params` text COMMENT '参数',\r\n" + 
					"  `response` text COMMENT '结果',\r\n" + 
					"  `is_use` int(11) NOT NULL COMMENT '是否使用',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '新增时间',\r\n" + 
					"  `update_time` datetime NOT NULL COMMENT '更新时间',\r\n" + 
					"  PRIMARY KEY  (`prj_id`,`path`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目的API表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createPrjAnt(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from prj_ant limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[prj_ant]: " + e.getMessage());
			String sql = "CREATE TABLE `prj_ant` (\r\n" + 
					"  `pa_id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `name` varchar(50) NOT NULL COMMENT '名称',\r\n" + 
					"  `pid` int(11) NOT NULL COMMENT '父编号',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `img` varchar(150) default NULL COMMENT '图片',\r\n" + 
					"  `api_text` text COMMENT 'api内容',\r\n" + 
					"  PRIMARY KEY  (`pa_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目原型表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createMsSecretApi(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from ms_secret_api limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[ms_secret_api]: " + e.getMessage());
			String sql = "CREATE TABLE `ms_secret_api` (\r\n" + 
					"  `cli_id` varchar(32) NOT NULL COMMENT '客户端编号',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `prj_code` varchar(50) NOT NULL COMMENT '项目编码',\r\n" + 
					"  `url` varchar(150) NOT NULL COMMENT 'API地址',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  PRIMARY KEY  (`cli_id`,`prj_id`,`url`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='密钥API权限表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createMsSecret(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from ms_secret limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[ms_secret]: " + e.getMessage());
			String sql = "CREATE TABLE `ms_secret` (\r\n" + 
					"  `cli_id` varchar(32) NOT NULL default '' COMMENT '客户端编号',\r\n" + 
					"  `name` varchar(100) NOT NULL COMMENT '名称',\r\n" + 
					"  `remark` varchar(100) default NULL COMMENT '备注',\r\n" + 
					"  `token` varchar(100) NOT NULL COMMENT '密钥',\r\n" + 
					"  `domain` varchar(200) default NULL COMMENT '主路径',\r\n" + 
					"  `is_use` int(11) NOT NULL COMMENT '是否使用',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `req_max_hour` bigint(20) NOT NULL default '0' COMMENT '每小时最大请求[0代表不限制]',\r\n" + 
					"  `req_max_second` bigint(20) NOT NULL default '0' COMMENT '每秒最大请求数[0代表不限制]',\r\n" + 
					"  PRIMARY KEY  (`cli_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用密钥表';";
			sql += "insert into `ms_secret` (`cli_id`, `name`, `remark`, `token`, `domain`, `is_use`, `create_time`) values('143756892','config','配置系统的密钥','4307a39dfc7ecb783d6682f44s3911e2','','1','2017-06-11 12:00:40');\r\n" + 
					"insert into `ms_secret` (`cli_id`, `name`, `remark`, `token`, `domain`, `is_use`, `create_time`) values('145756897','biz-api','biz-api项目的密钥','6e09a39dfc7edb7w3d66f2f44s3913e2','','1','2017-06-11 12:05:45');";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createMsConfigValue(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from ms_config_value limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[ms_config_value]: " + e.getMessage());
			String sql = "CREATE TABLE `ms_config_value` (\r\n" + 
					"  `config_id` int(11) NOT NULL COMMENT '配置编号',\r\n" + 
					"  `code` varchar(150) NOT NULL COMMENT 'key的编码',\r\n" + 
					"  `value` varchar(250) default NULL COMMENT 'value',\r\n" + 
					"  `remark` varchar(250) default NULL COMMENT '备注',\r\n" + 
					"  `orderby` int(11) NOT NULL default '0' COMMENT '排序',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  PRIMARY KEY  (`config_id`,`code`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置文件值表';";
			sql += "insert  into `ms_config_value`(`config_id`,`code`,`value`,`remark`,`orderby`,`create_time`,`user_id`) values \r\n" + 
					"(1,'client.monitor.id','145756897','请求其它服务的唯一ID',27,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'client.monitor.token','6e09a39dfc7edb7w3d66f2f44s3913e2','请求其它服务的密钥',28,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'client.task.server.host','http://msTask:7280','定时任务的备用请求地址',30,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'client.task.server.serviceId','ms-cloud-task','定时任务的服务ID',29,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'code.source.path','D:/data/monitor/source','生成的源码存放路径',16,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'code.template.path','D:/data/monitor/template','生成源码的模板存放路径',15,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'eureka.client.serviceUrl.defaultZone','http://msRc:7200/eureka/','注册中心地址',3,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'eureka.instance.preferIpAddress','true','实例名称显示ip',2,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'jdbc1.driverClassName','com.mysql.jdbc.Driver','数据库连接信息',17,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'jdbc1.password','root','数据库连接信息',20,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'jdbc1.url','jdbc:mysql://127.0.0.1:3306/monitor?useUnicode=true&characterEncoding=UTF-8','数据库连接信息',18,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'jdbc1.username','root','数据库连接信息',19,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'multipart.maxFileSize','200MB','限制上传文件的大小',12,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'multipart.maxRequestSize','200MB','限制上传文件的大小',13,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'project.monitor.name','Monitor','项目的名称',14,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'redis.hosts','127.0.0.1:6379','redis 配置,配置 hosts多个用英文分号分隔',21,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'redis.keyPrefix','msm-','redis 配置,key的前缀',26,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'redis.maxIdle','500','redis 配置',23,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'redis.maxTotal','1000','redis 配置',24,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'redis.maxWaitMillis','100000','redis 配置',25,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'redis.password','','redis 配置',22,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'server.port','7250','服务端口',1,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'server.servlet.context-path','/','项目前缀',10,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'server.tomcat.max-threads','30','tomcat最大线程数',11,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.application.name','ms-cloud-monitor','application的名称',0,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.jackson.date-format','yyyy-MM-dd HH:mm:ss','日期展示格式',4,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.jackson.time-zone','GMT+8','时区',5,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.thymeleaf.cache','false','关闭thymeleaf缓存',8,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.thymeleaf.enabled','false','关闭thymeleaf模板',9,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.view.prefix','/WEB-INF/view/','spring视图前缀',6,'2019-03-23 16:00:18',1),\r\n" + 
					"(1,'spring.view.suffix','.jsp','spring视图后缀',7,'2019-03-23 16:00:18',1),\r\n" + 
					"(2,'client.monitor.id','145756897','请求其它服务的唯一ID',19,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'client.monitor.server.host','http://msMonitor:7250','Monitor的地址',21,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'client.monitor.token','6e09a39dfc7edb7w3d66f2f44s3913e2','请求其它服务的密钥',20,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'eureka.client.serviceUrl.defaultZone','http://msRc:7200/eureka/','注册中心地址',3,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'eureka.instance.preferIpAddress','true','实例名称显示ip',2,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'jdbc1.driverClassName','com.mysql.jdbc.Driver','数据库连接信息',15,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'jdbc1.password','root','数据库连接信息',18,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'jdbc1.url','jdbc:mysql://127.0.0.1:3306/task?useUnicode=true&characterEncoding=UTF-8','数据库连接信息',16,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'jdbc1.username','root','数据库连接信息',17,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'multipart.maxFileSize','200MB','限制上传文件的大小',12,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'multipart.maxRequestSize','200MB','限制上传文件的大小',13,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'project.monitor.name','Task','项目的名称',14,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'server.port','7280','服务端口',1,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'server.servlet.context-path','/','项目前缀',10,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'server.tomcat.max-threads','30','tomcat最大线程数',11,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.application.name','ms-cloud-task','application的名称',0,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.jackson.date-format','yyyy-MM-dd HH:mm:ss','日期展示格式',4,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.jackson.time-zone','GMT+8','时区',5,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.thymeleaf.cache','false','关闭thymeleaf缓存',8,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.thymeleaf.enabled','false','关闭thymeleaf模板',9,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.view.prefix','/WEB-INF/view/','spring视图前缀',6,'2019-03-23 21:27:34',1),\r\n" + 
					"(2,'spring.view.suffix','.jsp','spring视图后缀',7,'2019-03-23 21:27:34',1);";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createMsConfig(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from ms_config limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[ms_config]: " + e.getMessage());
			String sql = "CREATE TABLE `ms_config` (\r\n" + 
					"  `config_id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `name` varchar(100) NOT NULL COMMENT '文件名称',\r\n" + 
					"  `remark` varchar(250) default NULL COMMENT '备注',\r\n" + 
					"  `is_use` int(11) NOT NULL COMMENT '是否使用',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建日期',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  `prj_id` int(11) NOT NULL default '0' COMMENT '项目编号',\r\n" + 
					"  PRIMARY KEY  (`config_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置文件表';";
			sql += "insert  into `ms_config`(`config_id`,`name`,`remark`,`is_use`,`create_time`,`user_id`,`prj_id`) values \r\n" + 
					"(1,'ms-cloud-monitor.properties','微服务的Monitor配置文件',1,'2019-03-23 15:44:14',1,0),\r\n" + 
					"(2,'ms-cloud-task.properties','定时任务的配置文件',1,'2019-03-23 21:24:56',1,0);";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createCodeTemplate(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from code_template limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[code_template]: " + e.getMessage());
			String sql = "CREATE TABLE `code_template` (\r\n" + 
					"  `code` varchar(50) NOT NULL COMMENT '源码编号',\r\n" + 
					"  `type` int(11) NOT NULL COMMENT '类型[10java、20jsp、30其它文件]',\r\n" + 
					"  `name` varchar(100) NOT NULL COMMENT '名称',\r\n" + 
					"  `remark` varchar(200) default NULL COMMENT '描叙',\r\n" + 
					"  `package_name` varchar(30) NOT NULL COMMENT '包名',\r\n" + 
					"  `content` text NOT NULL COMMENT '模板内容',\r\n" + 
					"  `path` varchar(200) NOT NULL COMMENT '模板路劲',\r\n" + 
					"  `suffix` varchar(50) default NULL COMMENT '文件后缀',\r\n" + 
					"  PRIMARY KEY  (`code`,`name`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目模板表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createCodePrj(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from code_prj limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[code_prj]: " + e.getMessage());
			String sql = "CREATE TABLE `code_prj` (\r\n" + 
					"  `code` varchar(50) NOT NULL COMMENT '编码',\r\n" + 
					"  `prj_id` int(11) NOT NULL COMMENT '项目编号',\r\n" + 
					"  `name` varchar(150) NOT NULL COMMENT '名称',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '创建人',\r\n" + 
					"  PRIMARY KEY  (`code`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目源码表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	private static void createCodeCreate(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from code_create limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[code_create]: " + e.getMessage());
			String sql = "CREATE TABLE `code_create` (\r\n" + 
					"  `id` int(11) NOT NULL auto_increment COMMENT '编号',\r\n" + 
					"  `code` varchar(50) NOT NULL COMMENT '源码编号',\r\n" + 
					"  `package_path` varchar(200) NOT NULL COMMENT '功能包路径',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[10待生成、20生成中、30生成失败、40生成成功]',\r\n" + 
					"  `download` varchar(200) default NULL COMMENT '下载地址',\r\n" + 
					"  `finish_time` datetime default NULL COMMENT '生成完成时间',\r\n" + 
					"  `ds_code` varchar(100) NOT NULL COMMENT '数据源编号',\r\n" + 
					"  `db_name` varchar(100) NOT NULL COMMENT '数据库名',\r\n" + 
					"  `tables` varchar(200) NOT NULL COMMENT '生成的表集合[多个,分隔]',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '创建时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '创建人',\r\n" + 
					"  PRIMARY KEY  (`id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生成源码表';";
			batchExec(sql, baseDao);
			LOGGER.info("|===================================================");
		}
	}
	
	private static void createCliInfo(BaseDao baseDao) {
		try {
			baseDao.queryForLong("select count(*) from cli_info limit 1");
		} catch (Exception e) {
			LOGGER.info("|===================================================");
			LOGGER.error("|========== 不存在表[cli_info]: " + e.getMessage());
			String sql = "CREATE TABLE `cli_info` (\r\n" + 
					"  `client_id` varchar(32) NOT NULL COMMENT '客户端编号',\r\n" + 
					"  `name` varchar(50) NOT NULL COMMENT '名称',\r\n" + 
					"  `remark` varchar(200) default NULL COMMENT '描叙',\r\n" + 
					"  `ip` varchar(30) NOT NULL COMMENT 'ip地址',\r\n" + 
					"  `port` int(11) NOT NULL COMMENT '端口',\r\n" + 
					"  `token` varchar(100) NOT NULL COMMENT '密钥',\r\n" + 
					"  `create_time` datetime NOT NULL COMMENT '添加时间',\r\n" + 
					"  `user_id` int(11) NOT NULL COMMENT '添加人',\r\n" + 
					"  `status` int(11) NOT NULL COMMENT '状态[10正常、20停用]',\r\n" + 
					"  `activity_status` int(11) default NULL COMMENT '活动状态[10正常、20心跳异常]',\r\n" + 
					"  `activity_time` datetime default NULL COMMENT '上次活动时间',\r\n" + 
					"  PRIMARY KEY  (`client_id`)\r\n" + 
					") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端表';";
			batchExec(sql, baseDao);
			/*
			sql = "alter table sys_user comment '用户表'";
			baseDao.updateSql(sql);
			sql = "create unique index unique_username on sys_user\r\n" + 
					"(\r\n" + 
					"   username\r\n" + 
					")";
			baseDao.updateSql(sql);
			String pwd = FrameMd5Util.getInstance().encodePassword("123456", "1");
			sql = "INSERT INTO `sys_user`(`id`,`username`,`password`,`nickname`,`addtime`,`adduser`,`status`)\r\n" + 
					" VALUES (1,'admin','" + pwd + "','管理员',now(),1,0)";
			baseDao.saveSql(sql);*/
			LOGGER.info("|===================================================");
		}
	}
	
	private static void batchExec(String sql, BaseDao baseDao) {
		List<String> sqls = FrameStringUtil.toArray(sql, ";");
		for (String s : sqls) {
			if (FrameStringUtil.isEmpty(s)) {
				continue;
			}
			baseDao.updateSql(s);
		}
	}
}
