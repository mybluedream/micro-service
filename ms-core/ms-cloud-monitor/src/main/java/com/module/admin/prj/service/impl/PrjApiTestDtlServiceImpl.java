package com.module.admin.prj.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.prj.dao.PrjApiTestDtlDao;
import com.module.admin.prj.enums.PrjApiTestDtlStatus;
import com.module.admin.prj.pojo.PrjApiTestDtl;
import com.module.admin.prj.service.PrjApiTestDtlService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api_test_dtl的Service
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
@Component
public class PrjApiTestDtlServiceImpl implements PrjApiTestDtlService {

	@Autowired
	private PrjApiTestDtlDao prjApiTestDtlDao;
	
	@Override
	public ResponseFrame saveOrUpdate(PrjApiTestDtl prjApiTestDtl) {
		ResponseFrame frame = new ResponseFrame();
		PrjApiTestDtl org = get(prjApiTestDtl.getPatId(), prjApiTestDtl.getPath());
		if(org == null) {
			prjApiTestDtl.setStatus(PrjApiTestDtlStatus.WAIT.getCode());
			prjApiTestDtlDao.save(prjApiTestDtl);
		} else {
			prjApiTestDtlDao.update(prjApiTestDtl);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public PrjApiTestDtl get(Integer patId, String path) {
		return prjApiTestDtlDao.get(patId, path);
	}

	@Override
	public ResponseFrame pageQuery(PrjApiTestDtl prjApiTestDtl) {
		prjApiTestDtl.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = prjApiTestDtlDao.findPrjApiTestDtlCount(prjApiTestDtl);
		List<PrjApiTestDtl> data = null;
		if(total > 0) {
			data = prjApiTestDtlDao.findPrjApiTestDtl(prjApiTestDtl);
			for (PrjApiTestDtl patd : data) {
				patd.setStatusName(PrjApiTestDtlStatus.getText(patd.getStatus()));
			}
		}
		Page<PrjApiTestDtl> page = new Page<PrjApiTestDtl>(prjApiTestDtl.getPage(), prjApiTestDtl.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(Integer patId, String path) {
		ResponseFrame frame = new ResponseFrame();
		prjApiTestDtlDao.delete(patId, path);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<PrjApiTestDtl> findByPatId(Integer patId) {
		return prjApiTestDtlDao.findByPatId(patId);
	}

	@Override
	public void updateStatusByPatId(Integer patId, Integer status) {
		prjApiTestDtlDao.updateStatusByPatId(patId, status);
	}

	@Override
	public void updateStatus(Integer patId, String path, Integer status,
			String testResult) {
		prjApiTestDtlDao.updateStatus(patId, path, status, testResult);
	}
}