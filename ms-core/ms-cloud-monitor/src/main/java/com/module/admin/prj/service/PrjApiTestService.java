package com.module.admin.prj.service;

import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjApiTest;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api_test的Service
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
@Component
public interface PrjApiTestService {
	
	/**
	 * 保存或修改
	 * @param prjApiTest
	 * @return
	 */
	public ResponseFrame saveOrUpdate(PrjApiTest prjApiTest);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public PrjApiTest get(Integer id);

	/**
	 * 分页获取对象
	 * @param prjApiTest
	 * @return
	 */
	public ResponseFrame pageQuery(PrjApiTest prjApiTest);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(Integer id);

	public ResponseFrame test(Integer id);
}