package com.git.test;

import com.module.comm.utils.JGitUtil;

/**
 * 操作git
 * @author yuejing
 * @date 2019年3月23日 上午11:07:56
 * @version V1.0.0
 */
public class GitTest {

	public static void main(String[] args) {
		String localRepoPath = "D://data//monitor//project";
		String remoteRepoUri = "https://gitee.com/yuejing/suyunyou-spider.git";
		String branchName = "master";
		String gitUsername = "";
		String gitPassword = "";
		// 创建仓库
		JGitUtil git = new JGitUtil(localRepoPath, remoteRepoUri, branchName, gitUsername, gitPassword);
		boolean checkout = git.checkout();
		System.out.println("checkout状态: " + checkout);
	}

}
