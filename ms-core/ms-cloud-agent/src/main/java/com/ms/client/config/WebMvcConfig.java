package com.ms.client.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.ms.client.interceptor.AuthSecurityInterceptor;

/**
 * WebMvc的配置
 * @author yuejing
 * @date 2019年3月5日 下午1:15:08
 */
@Configuration
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 添加拦截器
		registry.addInterceptor(new AuthSecurityInterceptor())
		.addPathPatterns("/*", "/*/*").excludePathPatterns("/refresh");
		WebMvcConfigurer.super.addInterceptors(registry);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/view/**").addResourceLocations("/view/");
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}
}
