
insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.smtp','发送邮箱的smtp','smtp.163.com',null,null,null);
insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.from','发送邮件的邮箱','xxx@163.com',null,null,null);
insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.username','发送邮件的用户名','xxxx',null,null,null);
insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.password','发送邮件的密码','xxxxxx',null,null,null);
insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('mail.send.is.open','是否打开发送邮件的功能[0否、1是]','0',null,null,null);

insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('prj.file.path','项目上传的目录','/home/monitor/file',null,null,null);
insert into `sys_config` (`code`, `name`, `value`, `remark`, `exp1`, `exp2`) values('prj.monitor.fail.email','项目检测失败接收的邮箱','yuejingjiahong@163.com',null,null,null);

insert  into `sys_user`(`user_id`,`username`,`password`,`nickname`,`create_time`,`status`) values (1,'admin','43286a86708820e38c333cdd4c496355','admin','2016-10-19 10:53:38',10);

insert into `ms_secret` (`cli_id`, `name`, `remark`, `token`, `domain`, `is_use`, `create_time`) values('143756892','config','配置系统的密钥','4307a39dfc7ecb783d6682f44s3911e2','','1','2017-06-11 12:00:40');
insert into `ms_secret` (`cli_id`, `name`, `remark`, `token`, `domain`, `is_use`, `create_time`) values('145756897','biz-api','biz-api项目的密钥','6e09a39dfc7edb7w3d66f2f44s3913e2','','1','2017-06-11 12:05:45');

insert  into `ms_config`(`config_id`,`name`,`remark`,`is_use`,`create_time`,`user_id`,`prj_id`) values 
(1,'ms-cloud-monitor.properties','微服务的Monitor配置文件',1,'2019-03-23 15:44:14',1,0),
(2,'ms-cloud-task.properties','定时任务的配置文件',1,'2019-03-23 21:24:56',1,0);

insert  into `ms_config_value`(`config_id`,`code`,`value`,`remark`,`orderby`,`create_time`,`user_id`) values 
(1,'client.monitor.id','145756897','请求其它服务的唯一ID',27,'2019-03-23 16:00:18',1),
(1,'client.monitor.token','6e09a39dfc7edb7w3d66f2f44s3913e2','请求其它服务的密钥',28,'2019-03-23 16:00:18',1),
(1,'client.task.server.host','http://msTask:7280','定时任务的备用请求地址',30,'2019-03-23 16:00:18',1),
(1,'client.task.server.serviceId','ms-cloud-task','定时任务的服务ID',29,'2019-03-23 16:00:18',1),
(1,'code.source.path','D:/data/monitor/source','生成的源码存放路径',16,'2019-03-23 16:00:18',1),
(1,'code.template.path','D:/data/monitor/template','生成源码的模板存放路径',15,'2019-03-23 16:00:18',1),
(1,'eureka.client.serviceUrl.defaultZone','http://msRc:7200/eureka/','注册中心地址',3,'2019-03-23 16:00:18',1),
(1,'eureka.instance.preferIpAddress','true','实例名称显示ip',2,'2019-03-23 16:00:18',1),
(1,'jdbc1.driverClassName','com.mysql.jdbc.Driver','数据库连接信息',17,'2019-03-23 16:00:18',1),
(1,'jdbc1.password','root','数据库连接信息',20,'2019-03-23 16:00:18',1),
(1,'jdbc1.url','jdbc:mysql://127.0.0.1:3306/monitor?useUnicode=true&characterEncoding=UTF-8','数据库连接信息',18,'2019-03-23 16:00:18',1),
(1,'jdbc1.username','root','数据库连接信息',19,'2019-03-23 16:00:18',1),
(1,'multipart.maxFileSize','200MB','限制上传文件的大小',12,'2019-03-23 16:00:18',1),
(1,'multipart.maxRequestSize','200MB','限制上传文件的大小',13,'2019-03-23 16:00:18',1),
(1,'project.monitor.name','Monitor','项目的名称',14,'2019-03-23 16:00:18',1),
(1,'redis.hosts','127.0.0.1:6379','redis 配置,配置 hosts多个用英文分号分隔',21,'2019-03-23 16:00:18',1),
(1,'redis.keyPrefix','msm-','redis 配置,key的前缀',26,'2019-03-23 16:00:18',1),
(1,'redis.maxIdle','500','redis 配置',23,'2019-03-23 16:00:18',1),
(1,'redis.maxTotal','1000','redis 配置',24,'2019-03-23 16:00:18',1),
(1,'redis.maxWaitMillis','100000','redis 配置',25,'2019-03-23 16:00:18',1),
(1,'redis.password','','redis 配置',22,'2019-03-23 16:00:18',1),
(1,'server.port','7250','服务端口',1,'2019-03-23 16:00:18',1),
(1,'server.servlet.context-path','/','项目前缀',10,'2019-03-23 16:00:18',1),
(1,'server.tomcat.max-threads','30','tomcat最大线程数',11,'2019-03-23 16:00:18',1),
(1,'spring.application.name','ms-cloud-monitor','application的名称',0,'2019-03-23 16:00:18',1),
(1,'spring.jackson.date-format','yyyy-MM-dd HH:mm:ss','日期展示格式',4,'2019-03-23 16:00:18',1),
(1,'spring.jackson.time-zone','GMT+8','时区',5,'2019-03-23 16:00:18',1),
(1,'spring.thymeleaf.cache','false','关闭thymeleaf缓存',8,'2019-03-23 16:00:18',1),
(1,'spring.thymeleaf.enabled','false','关闭thymeleaf模板',9,'2019-03-23 16:00:18',1),
(1,'spring.view.prefix','/WEB-INF/view/','spring视图前缀',6,'2019-03-23 16:00:18',1),
(1,'spring.view.suffix','.jsp','spring视图后缀',7,'2019-03-23 16:00:18',1),
(2,'client.monitor.id','145756897','请求其它服务的唯一ID',19,'2019-03-23 21:27:34',1),
(2,'client.monitor.server.host','http://msMonitor:7250','Monitor的地址',21,'2019-03-23 21:27:34',1),
(2,'client.monitor.token','6e09a39dfc7edb7w3d66f2f44s3913e2','请求其它服务的密钥',20,'2019-03-23 21:27:34',1),
(2,'eureka.client.serviceUrl.defaultZone','http://msRc:7200/eureka/','注册中心地址',3,'2019-03-23 21:27:34',1),
(2,'eureka.instance.preferIpAddress','true','实例名称显示ip',2,'2019-03-23 21:27:34',1),
(2,'jdbc1.driverClassName','com.mysql.jdbc.Driver','数据库连接信息',15,'2019-03-23 21:27:34',1),
(2,'jdbc1.password','root','数据库连接信息',18,'2019-03-23 21:27:34',1),
(2,'jdbc1.url','jdbc:mysql://127.0.0.1:3306/task?useUnicode=true&characterEncoding=UTF-8','数据库连接信息',16,'2019-03-23 21:27:34',1),
(2,'jdbc1.username','root','数据库连接信息',17,'2019-03-23 21:27:34',1),
(2,'multipart.maxFileSize','200MB','限制上传文件的大小',12,'2019-03-23 21:27:34',1),
(2,'multipart.maxRequestSize','200MB','限制上传文件的大小',13,'2019-03-23 21:27:34',1),
(2,'project.monitor.name','Task','项目的名称',14,'2019-03-23 21:27:34',1),
(2,'server.port','7280','服务端口',1,'2019-03-23 21:27:34',1),
(2,'server.servlet.context-path','/','项目前缀',10,'2019-03-23 21:27:34',1),
(2,'server.tomcat.max-threads','30','tomcat最大线程数',11,'2019-03-23 21:27:34',1),
(2,'spring.application.name','ms-cloud-task','application的名称',0,'2019-03-23 21:27:34',1),
(2,'spring.jackson.date-format','yyyy-MM-dd HH:mm:ss','日期展示格式',4,'2019-03-23 21:27:34',1),
(2,'spring.jackson.time-zone','GMT+8','时区',5,'2019-03-23 21:27:34',1),
(2,'spring.thymeleaf.cache','false','关闭thymeleaf缓存',8,'2019-03-23 21:27:34',1),
(2,'spring.thymeleaf.enabled','false','关闭thymeleaf模板',9,'2019-03-23 21:27:34',1),
(2,'spring.view.prefix','/WEB-INF/view/','spring视图前缀',6,'2019-03-23 21:27:34',1),
(2,'spring.view.suffix','.jsp','spring视图后缀',7,'2019-03-23 21:27:34',1);